# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# AdminStamp
This gem builds on the styling of Active Admin's. It is not a complete rework, yet, and as such still relies on AA's styles for a lot of things.

## Usage
All variables that are in admin_stamp.scss are presented in active_admin.scss in the assets directory. Change these to change the logo, type, etc.

Be aware when changing admin_stamp.scss that many things might fall back to AA styling, or other gem's styling. Try to parse out as many things as you can to Variables, that can then be incorporated to active_admin.scss and thus changed at will.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'admin_stamp'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install admin_stamp
```

Lastly, to install
```bash
$ rails g admin_stamp:install
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
