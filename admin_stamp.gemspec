$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "admin_stamp/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "admin_stamp"
  s.version     = AdminStamp::VERSION
  s.authors     = ["Peter Binkowski"]
  s.email       = ["peter@neotericdesign.com"]
  s.homepage    = "https://www.neotericdesign.com"
  s.summary     = "Branding and skinning for ActiveAdmin"
  s.description = "Branding and skinning for ActiveAdmin"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile",
                "README.md", "CHANGELOG.md"]

  s.add_dependency "rails", ">= 4.2"

  s.add_development_dependency "sqlite3"
end
