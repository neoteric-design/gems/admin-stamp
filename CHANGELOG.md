0.1.1
=====

* Initial release

0.1.4
=====

* Minor bump, fixed logo sizing, and some border radiuses, and action bar.

0.2
=====

* Fix filters as a button, overall tightening of space and type for a refresh

0.2.2
=====

* tweak sub-nav popouts


0.2.5
=====

* better responsive positioning and sizing on title bar and main content area.


0.2.6
=====

* Hide the status bar
