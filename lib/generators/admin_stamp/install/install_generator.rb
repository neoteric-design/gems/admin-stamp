module AdminStamp
  module ActiveAdminAssets
    def insert_into_css_file(content, **options)
      return unless css_file
      options[:after] ||= %r{@import "active_admin/base";}
      insert_into_asset_file css_file, content, options
    end

    def insert_into_js_file(content, **options)
      content.gsub!(/^\/\//, '#') if js_file.ends_with?('coffee')
      content.gsub!(/^#/, '//') if js_file.ends_with?('js')

      options[:after] ||= %r{(//)|#= require +['"]?active_admin/base['"]?}
      insert_into_asset_file js_file, content, options
    end

    private

    def js_file
      @js_file ||= find_existing_file(potential_js_files)
    end

    def css_file
      @css_file ||= find_existing_file(potential_css_files)
    end

    def find_existing_file(files)
      files.detect { |file| File.exist?(file) }
    end

    def insert_into_asset_file(asset_file, content, **options)
      return unless asset_file
      if File.binread(asset_file).include?(content)
        say_status("skipped", "insert into #{asset_file}", :yellow)
      else
        insert_into_file asset_file, "\n#{content}", options
      end
    end

    def potential_js_files
      [
        "app/assets/javascripts/active_admin.coffee",
        "app/assets/javascripts/active_admin.js.coffee",
        "app/assets/javascripts/active_admin.js"
      ]
    end

    def potential_css_files
      [
        "app/assets/stylesheets/active_admin.scss",
        "app/assets/stylesheets/active_admin.css.scss",
        "app/assets/stylesheets/active_admin.css"
      ]
    end
  end


  class InstallGenerator < Rails::Generators::Base
    include ActiveAdminAssets

    def config_activeadmin
      insert_into_file 'config/initializers/active_admin.rb',
                       "\n  config.view_factory.footer = AdminStamp::Components::Footer",
                       after: 'ActiveAdmin.setup do |config|'
    end

    def require_css
      prepend_to_file css_file, <<~STAMP_VARIABLES
      // ActiveStamp Customization /////////////////////////////////////////

      $stampTextColor: #1e2a33 !default;
      $stampTextActiveColor: #fff !default;
      $stampTextTable: #7f8c8d !default;
      $stampActiveColor: #e73c3c !default;
      $stampHeaderBck: #1e2a33 !default;
      $stampHeaderTextColor: #FFFFFF !default;
      $stampFontFamily: 'Source Sans Pro', sans-serif !default;
      $panelHeaderBck: #2c3e50 !default;
      $panelHeaderTextColor: #FFFFFF !default;
      $stampLogo: #fff image-url("admin_stamp/ND-logo-square.png") no-repeat 30px 0 !default;

      //////////////////////////////////////////////////////////////////////

      STAMP_VARIABLES

      insert_into_css_file '@import "admin_stamp";'
    end
  end
end
