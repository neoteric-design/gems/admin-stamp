require 'active_admin'
module AdminStamp
  module Components
    class Footer < ::ActiveAdmin::Views::Footer
      private

      def powered_by_message
        "Neoteric CMS v.7 - #{Time.zone.today.year}"
      end
    end
  end
end